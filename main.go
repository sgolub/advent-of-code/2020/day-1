package main

import (
	"fmt"

	"gitlab.com/sgolub/advent-of-code/base/v2/days"
)

type myDay struct {
	data map[string]string
}

func (d myDay) SetData(key, value string) {
	d.data[key] = value
}

// Year will return the AOC Year
func (d myDay) Year() int {
	return 2020
}

// Day will return the day number for this puzzle
func (d myDay) Day() int {
	return 1
}

// GetData will load the data and parse it from disk
func (d myDay) GetData(useSampleData bool) []int {
	result := []int{}
	data, ok := days.Control().LoadData(d, useSampleData)["input"]
	if !ok {
		return []int{}
	}
	for _, i := range data.([]interface{}) {
		result = append(result, i.(int))
	}
	return result
}

// Solution1 is the solution to the first part of the puzzle
func (d myDay) Solution1(useSample bool) string {
	defer days.NewTimer("D1P1")
	data := d.GetData(useSample)
	for i, num1 := range data {
		for j, num2 := range data {
			if i == j {
				continue
			}
			if num1+num2 == 2020 {
				return fmt.Sprintf("%d", num1*num2)
			}
		}
	}
	return ""
}

// Solution2 is the solution to the second part of the puzzle
func (d myDay) Solution2(useSample bool) string {
	defer days.NewTimer("D1P2")
	data := d.GetData(useSample)
	for i, num1 := range data {
		for j, num2 := range data {
			if i == j {
				continue
			}
			for k, num3 := range data {
				if i == k || j == k {
					continue
				}
				if num1+num2+num3 == 2020 {
					return fmt.Sprintf("%d", num1*num2*num3)
				}
			}
		}
	}
	return ""
}

// GetDay will return this module's day. Used by the plugin loader.
func GetDay() days.Day {
	return myDay{
		data: map[string]string{},
	}
}
